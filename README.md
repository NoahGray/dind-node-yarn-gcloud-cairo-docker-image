# DinD, Node, Yarn, gCloud & Kubectl and Cairo Docker image

This is for making Docker images from within Docker, where you may also need gCloud and Kubectl to deploy the image to Kubernetes. It includes Cairo as well, if your app needs Canvas on the back-end. It is for Node projects and include yarn.