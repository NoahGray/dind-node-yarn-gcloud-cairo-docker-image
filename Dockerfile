FROM docker:dind

MAINTAINER Noah <noahgray@me.com>

RUN apk update && apk upgrade && \
  apk add --no-cache \
    nodejs \
    bash \
    cairo-dev \
    jpeg-dev \
    giflib-dev \
    pango-dev \
    libpng-dev \
    librsvg-dev \
    alpine-sdk \
    libexecinfo-dev \
    python2 \
    automake \
    autoconf \
    jq \
    yarn

RUN yarn global add node-gyp

RUN mkdir -p /builds

# Install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

# Install gcloud
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-238.0.0-linux-x86_64.tar.gz
RUN tar zxvf google-cloud-sdk-238.0.0-linux-x86_64.tar.gz google-cloud-sdk
ENV PATH /google-cloud-sdk/bin:$PATH

RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

ENV LC_ALL=en_US.UTF-8
